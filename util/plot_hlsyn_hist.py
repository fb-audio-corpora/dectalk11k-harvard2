#!/usr/bin/env python3
# -*- encoding: utf-8 -*-
# vim: set fileencoding=utf-8
#
# author: jan 2020
# cassio batista - https://cassota.gitlab.io/

import sys
import os
import glob
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.ticker import FuncFormatter
from collections import OrderedDict

from hlsyn_handler import *

DEBUG = False
NUM_HIST_BINS = 10
SHOW_BOXPLOT = True
SHOW_AVG_LINE = True
SHOW_STD_LINE = True

MALE_SPEAKERS = ['paul', 'harry', 'frank', 'dennis']
FEMALE_SPEAKERS = ['betty', 'kit', 'rita', 'ursula', 'wendy']

SRC_DIR = os.path.dirname(os.path.abspath(__file__))
ROOT_DIR = os.path.abspath(os.path.dirname(SRC_DIR))

TAG = os.path.basename(sys.argv[0])


def create_mem_matrix(target, data_dir):
    mem_mtx = np.zeros((1, 13))
    if target == 'female':
        if DEBUG:
            spk_list = FEMALE_SPEAKERS[:1]
        else:
            spk_list = FEMALE_SPEAKERS
    elif target == 'male':
        if DEBUG:
            spk_list = MALE_SPEAKERS[:1]
        else:
            spk_list = MALE_SPEAKERS
    else:
        print('[%s] error: unrecognised target "%s"', (TAG, target))
        return None
    for spk in spk_list:
        spk_path = os.path.join(data_dir, spk)
        print('[%s] info: %s: scanning "%s"' % (TAG, target, spk_path))
        for mem_file in glob.glob(os.path.join(spk_path, '*.hlsyn')):
            with open(mem_file, 'r') as f:
                lines = (line for line in f if not line.startswith('AG'))
                mem_mtx = np.concatenate((mem_mtx, np.loadtxt(lines, skiprows=1)))
    return mem_mtx.T  # one param per mtx line


# https://matplotlib.org/examples/pylab_examples/custom_ticker1.html
def thou_y(y, pos):
    if y >= 1000:
        return '%1.0fK' % (y / 1000)
    return '%d' % int(y)


def mill_y(y, pos):
    return '%1.0fM' % (y / 1000000)


def milli_x(x, pos):
    if x == 0.0:
        return '%d' % int(x)
    x_str = '%.2f' % (x * 1000)
    return x_str.strip('0').replace('-0.', '-.') + 'm'


def thou_x(x, pos):
    if x >= 1000 or x <= -1000:
        return '%1.0fK' % (x / 1000)
    return '%d' % int(x)


def leg_fmt(x):
    if x < 100 and x > -100:
        return '%1.2f' % x
    elif x >= 1000 or x <= -1000:
        return '%1.2fK' % (x / 1000)
    else:
        return '%d' % int(x)


def build_transpose_indexes(rows, cols):
    dim = rows * cols
    indexes = list(range(1, dim + 1))
    indexes_T = np.array(indexes).reshape((rows, cols)).T.flatten()
    return list(indexes_T)


def plot_hist(mtx_m, mtx_f, ps):
    fig = plt.figure()
    plt.title(ps.title, fontsize=14)
    plt.axis('off')
    rows, cols = ps.rows, ps.cols
    for i, param_index in enumerate(ps.param_list):
        sys.stderr.write('  debug: subplot %-2d / %-2d (%-2d) ' %
                         (i + 1, ps.num_params, param_index))
        ax = fig.add_subplot(rows, cols, i + 1)
        array_m = mtx_m[param_index]
        array_f = mtx_f[param_index]
        mean_m, stddev_m = np.mean(array_m), np.std(array_m)
        mean_f, stddev_f = np.mean(array_f), np.std(array_f)
        sys.stderr.write('%-10s ' % PARAMS[param_index])
        sys.stderr.write('%20.8f %20.8f %20.8f %20.8f\t| ' %
                         (mean_m, stddev_m, array_m.min(), array_m.max()))
        sys.stderr.write('%20.8f %20.8f %20.8f %20.8f\n' %
                         (mean_f, stddev_f, array_f.min(), array_f.max()))
        sys.stderr.flush()
        hist_m = ax.hist(array_m, bins=NUM_HIST_BINS, fc=PRIM_COLOR['male'], 
              alpha=TRANSPAR['male'], ec=PRIM_COLOR['male'])
        hist_f = ax.hist(array_f, bins=NUM_HIST_BINS, fc=PRIM_COLOR['female'], 
                alpha=TRANSPAR['female'], ec=PRIM_COLOR['female'])
        if SHOW_STD_LINE:
            ax.errorbar(x=mean_m,
                        y=np.mean(ax.get_ylim()) - 0 * np.mean(ax.get_ylim())/4,
                        xerr=stddev_m, capsize=4, ecolor=SEC_COLOR['male'], 
                        fmt='--')
            ax.errorbar(x=mean_f,
                        y=np.mean(ax.get_ylim()) - 2 * np.mean(ax.get_ylim())/4,
                        xerr=stddev_f, capsize=4, ecolor=SEC_COLOR['female'],
                        fmt='--')
        if SHOW_AVG_LINE:
            ax.plot(mean_m,
                    np.mean(ax.get_ylim()) - 0 * np.mean(ax.get_ylim())/4,
                    linestyle='None', color=SEC_COLOR['male'], marker='o',
                    markerfacecolor=SEC_COLOR['male'], markersize=3,
                    label=r'$\mu\approx$%s, %s' % (leg_fmt(mean_m), leg_fmt(mean_f)) +
                          '\n' +
                          r'$\sigma\approx$%s, %s' % (leg_fmt(stddev_m), leg_fmt(stddev_f)))
            leg_1_fc = 'white'
            #if mean_m > 0.0 and mean_f > 0.0:
            #    if (stddev_m * 100.0 / mean_m) < 1.0 and \
            #            (stddev_f * 100 / mean_f) < 1.0:
            #        leg_1_fc = '#ff3333'
            #        for axis in ['bottom', 'top', 'left', 'right']:
            #            ax.spines[axis].set_linewidth(2.2); 
            #            ax.spines[axis].set_color(leg_1_fc)
            #elif stddev_m < 1.0 and stddev_f < 1.0:
            #        leg_1_fc = '#ff3333'
            #        for axis in ['bottom', 'top', 'left', 'right']:
            #            ax.spines[axis].set_linewidth(2.2); 
            #            ax.spines[axis].set_color(leg_1_fc)
            ax.plot(mean_f,
                    np.mean(ax.get_ylim()) - 2 * np.mean(ax.get_ylim())/4,
                    linestyle='None', color=SEC_COLOR['female'], marker='o',
                    markerfacecolor=SEC_COLOR['female'], markersize=3)
        if SHOW_BOXPLOT:
            pos = np.mean(ax.get_ylim()) - 1 * np.mean(ax.get_ylim())/4
            ax.boxplot(array_m, vert=False, widths=np.mean(ax.get_ylim())/6,
                       positions=[pos],
                       boxprops=dict(color=SEC_COLOR['male']),
                       capprops=dict(color=SEC_COLOR['male']),
                       whiskerprops=dict(color=SEC_COLOR['male']),
                       medianprops=dict(color=SEC_COLOR['male']),
                       flierprops=dict(marker='.', markersize=0.01, alpha=0.5,
                                       markerfacecolor=PRIM_COLOR['male'],
                                       markeredgecolor=PRIM_COLOR['male']))
            pos = np.mean(ax.get_ylim()) - 3 * np.mean(ax.get_ylim())/4
            ax.boxplot(array_f, vert=False, widths=np.mean(ax.get_ylim())/6,
                       positions=[pos],
                       boxprops=dict(color=SEC_COLOR['female']),
                       capprops=dict(color=SEC_COLOR['female']),
                       whiskerprops=dict(color=SEC_COLOR['female']),
                       medianprops=dict(color=SEC_COLOR['female']),
                       flierprops=dict(marker='.', markersize=0.01, alpha=0.5,
                                       markerfacecolor=PRIM_COLOR['female'],
                                       markeredgecolor=PRIM_COLOR['female']))
        # format yticks
        ax.set_yticks(np.arange(np.mean(ax.get_ylim()),
                                ax.get_ylim()[1]+1,
                                np.mean(ax.get_ylim())))
        ax.set_ylim(ax.get_ylim())
        plt.yticks(rotation='vertical')
        # format yaxis
        if ax.get_ylim()[1] > 1000000:
            ax.yaxis.set_major_formatter(FuncFormatter(mill_y))
        elif ax.get_ylim()[1] > 1000:
            ax.yaxis.set_major_formatter(FuncFormatter(thou_y))
        # format xaxis
        if ax.get_xlim()[1] >= 1000000 or ax.get_xlim()[0] <= -1000000:
            ax.xaxis.set_major_formatter(FuncFormatter(mill_y))
        elif ax.get_xlim()[1] >= 1000 or ax.get_xlim()[0] <= -1000:
            ax.xaxis.set_major_formatter(FuncFormatter(thou_x))
        elif ax.get_xlim()[1] < (1 / 1000):
            ax.xaxis.set_major_formatter(FuncFormatter(milli_x))
        # https://stackoverflow.com/a/38454818/3798300
        leg_1 = plt.legend([PARAMS[param_index].replace('formant_', 'F')],
                           markerscale=0, loc='upper right', handlelength=-0.8,
                           facecolor=leg_1_fc)
        leg_2 = ax.legend(markerscale=0, fontsize=9, loc='upper left',
                          handlelength=-0.8)
        ax.add_artist(leg_1)
    plt.subplots_adjust(left=0.025, right=0.975, top=0.950, bottom=0.05)
    fig.legend(handles=(hist_m[2][0], hist_f[2][0]), labels=('male', 'female'),
               ncol=2)
    plt.show(block=False)


if __name__ == '__main__':
    if len(sys.argv) != 2:
        print('usage: %s <target-gen> <data-dir>' % TAG)
        print('  <data-dir> dir where data for all speakers has been stored')
        sys.exit(1)

    data_dir = sys.argv[1]
    if not os.path.isdir(data_dir):
        print('[%s] error: "%s" is not a valid dir' % (TAG, data_dir))
        sys.exit(1)

    if DEBUG:
        print('[%s] info: running in debug mode' % TAG)
    if SHOW_BOXPLOT:
        print('[%s] info: boxplot will be shown over histogram' % TAG)
    if SHOW_AVG_LINE:
        print('[%s] info: mean will be plotted over the' % TAG, end=' ')
        print('histogram via plot() as a red vertical line')
    if SHOW_STD_LINE:
        print('[%s] info: standard deviation will be plotted' % TAG, end=' ')
        print('over histogram via errorplor() as a red horizontal line')

    mem_mtx_m = create_mem_matrix('male', data_dir)
    mem_mtx_f = create_mem_matrix('female', data_dir)
    for i, ps in enumerate(CONDENSED_PARAMS_MAP):
        print('[%s] info: plotting param set %d: %s' % (TAG, i + 1, ps.title))
        plot_hist(mem_mtx_m, mem_mtx_f, ps)
    x = input('[%s] info: done! type enter to close everything: ' % TAG)
