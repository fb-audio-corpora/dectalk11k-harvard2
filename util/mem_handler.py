#!/usr/bin/env python3
# -*- encoding: utf-8 -*-
# vim: set fileencoding=utf-8
#
# author: jan 2020
# cassio batista - https://cassota.gitlab.io/

PRIM_COLOR = { 'male': 'C0', 'female': 'C1' }
SEC_COLOR  = { 'male': 'b', 'female': 'r'}
TRANSPAR   = { 'male': 0.7, 'female': 0.3}
HATCH      = { 'male': '/', 'female': '\\' }  # TODO TBD


class ParamsSet:
    def __init__(self, rng_list, dim, title, transpose=False):
        self.ranges_list  = rng_list
        self.rows         = dim[0] 
        self.cols         = dim[1]
        self.dim          = self.rows * self.cols
        self.title        = title
        self.transpose    = transpose

        self.do_assert()

        self.param_list = self.gen_param_list()
        self.num_params = len(self.param_list)

    def do_assert(self):
        assert isinstance(self.ranges_list, list), \
                    'params index must be a LIST of tuples'
        assert isinstance(self.rows, int), \
                   'number of rows must be an INT'
        assert isinstance(self.cols, int), \
                    'number of cols must be an INT'
        assert isinstance(self.title, str), \
                    'title must be a string'

    def gen_param_list(self):
        param_list = []
        for rng in self.ranges_list:
            begin = rng[0]
            end = rng[1] + 1
            param_list += list(range(begin, end))
        return param_list

MEM_PARAMS = [
    # Synthesizer structure: static data */
    'state.parallel\_only\_flag',    # 0
    'state.num\_casc\_formants',     # 1
    'state.num\_samples',           # 2
    'state.output\_select',         # 3

    # Synthesizer structure: dynamic state data */
    'state.pulse\_freq',            # 4
    'state.glottis\_open',          # 5
    'state.period\_ctr',            # 6
    'state.voicing\_state',         # 7
    'state.pulse',                 # 8
    'state.random',                # 9
    'state.voicing\_time',          # 10
    'state.global\_time',           # 11
    'state.voicing\_amp',           # 12
    'state.glottal\_state',         # 13
    'state.asp\_state',             # 14
    'state.integrator',            # 15

    # Synthesizer structure: voicing state */
    'state.F0',                    # 16
    'state.FL',                    # 17
    'state.OQ',                    # 18
    'state.SQ',                    # 19
    'state.DI',                    # 20
    'state.AV',                    # 21
    'state.TL',                    # 22
    'state.close\_shortened',       # 23
    'state.close\_time',            # 24

    # Coefficients structure */
    'coefs.asp\_amp',               # 25
    'coefs.fric\_amp',              # 26
    'coefs.f1p\_amp',               # 27
    'coefs.f2p\_amp',               # 28
    'coefs.f3p\_amp',               # 29
    'coefs.f4p\_amp',               # 30
    'coefs.f5p\_amp',               # 31
    'coefs.f6p\_amp',               # 32
    'coefs.npv\_amp',               # 33
    'coefs.f1v\_amp',               # 34
    'coefs.f2v\_amp',               # 35
    'coefs.f3v\_amp',               # 36
    'coefs.f4v\_amp',               # 37
    'coefs.tpv\_amp',               # 38
    'coefs.bypass\_amp',            # 39

    # Speaker structure */
    'spkr.DU',                     # 40
    'spkr.UI',                     # 41
    'spkr.SR',                     # 42
    'spkr.NF',                     # 43
    'spkr.SS',                     # 44
    'spkr.RS',                     # 45
    'spkr.SB',                     # 46
    'spkr.CP',                     # 47
    'spkr.OS',                     # 48
    'spkr.GV',                     # 49
    'spkr.GH',                     # 50
    'spkr.GF',                     # 51

    # Resonator structure */
    'glottal\_pulse.Coef.A',        # 52
    'glottal\_pulse.Coef.B',        # 53
    'glottal\_pulse.Coef.C',        # 54
    'glottal\_pulse.State.Z1',      # 55
    'glottal\_pulse.State.Z2',      # 56

    'spectral\_tilt.Coef.A',        # 57
    'spectral\_tilt.Coef.B',        # 58
    'spectral\_tilt.Coef.C',        # 59
    'spectral\_tilt.State.Z1',      # 60
    'spectral\_tilt.State.Z2',      # 61

    'nasal\_pole\_cascade.Coef.A',   # 62
    'nasal\_pole\_cascade.Coef.B',   # 63
    'nasal\_pole\_cascade.Coef.C',   # 64 
    'nasal\_pole\_cascade.State.Z1', # 65
    'nasal\_pole\_cascade.State.Z2', # 66

    # Resonators: Formant Cascade 1 to 8
    'formant\_1\_cascade.Coef.A',    # 67
    'formant\_1\_cascade.Coef.B',    # 68 
    'formant\_1\_cascade.Coef.C',    # 69
    'formant\_1\_cascade.State.Z1',  # 70
    'formant\_1\_cascade.State.Z2',  # 71

    'formant\_2\_cascade.Coef.A',    # 72
    'formant\_2\_cascade.Coef.B',    # 73
    'formant\_2\_cascade.Coef.C',    # 74
    'formant\_2\_cascade.State.Z1',  # 75
    'formant\_2\_cascade.State.Z2',  # 76

    'formant\_3\_cascade.Coef.A',    # 77
    'formant\_3\_cascade.Coef.B',    # 78
    'formant\_3\_cascade.Coef.C',    # 79
    'formant\_3\_cascade.State.Z1',  # 80
    'formant\_3\_cascade.State.Z2',  # 81

    'formant\_4\_cascade.Coef.A',    # 82
    'formant\_4\_cascade.Coef.B',    # 83
    'formant\_4\_cascade.Coef.C',    # 84
    'formant\_4\_cascade.State.Z1',  # 85 
    'formant\_4\_cascade.State.Z2',  # 86 

    'formant\_5\_cascade.Coef.A',    # 87
    'formant\_5\_cascade.Coef.B',    # 88
    'formant\_5\_cascade.Coef.C',    # 89
    'formant\_5\_cascade.State.Z1',  # 90
    'formant\_5\_cascade.State.Z2',  # 91

    'formant\_6\_cascade.Coef.A',    # 92
    'formant\_6\_cascade.Coef.B',    # 93
    'formant\_6\_cascade.Coef.C',    # 94
    'formant\_6\_cascade.State.Z1',  # 95 
    'formant\_6\_cascade.State.Z2',  # 96 

    'formant\_7\_cascade.Coef.A',    # 97
    'formant\_7\_cascade.Coef.B',    # 98
    'formant\_7\_cascade.Coef.C',    # 99
    'formant\_7\_cascade.State.Z1',  # 100
    'formant\_7\_cascade.State.Z2',  # 101
    
    'formant\_8\_cascade.Coef.A',    # 102
    'formant\_8\_cascade.Coef.B',    # 103
    'formant\_8\_cascade.Coef.C',    # 104
    'formant\_8\_cascade.State.Z1',  # 105 
    'formant\_8\_cascade.State.Z2',  # 106 

    # Resonators: Formants Paralel 2 to 6
    'formant\_2\_parallel.Coef.A',   # 107
    'formant\_2\_parallel.Coef.B',   # 108
    'formant\_2\_parallel.Coef.C',   # 109
    'formant\_2\_parallel.State.Z1', # 110 
    'formant\_2\_parallel.State.Z2', # 111 
  
    'formant\_3\_parallel.Coef.A',   # 112
    'formant\_3\_parallel.Coef.B',   # 113
    'formant\_3\_parallel.Coef.C',   # 114
    'formant\_3\_parallel.State.Z1', # 115
    'formant\_3\_parallel.State.Z2', # 116

    'formant\_4\_parallel.Coef.A',   # 117
    'formant\_4\_parallel.Coef.B',   # 118
    'formant\_4\_parallel.Coef.C',   # 119
    'formant\_4\_parallel.State.Z1', # 120
    'formant\_4\_parallel.State.Z2', # 121

    'formant\_5\_parallel.Coef.A',   # 122
    'formant\_5\_parallel.Coef.B',   # 123
    'formant\_5\_parallel.Coef.C',   # 124
    'formant\_5\_parallel.State.Z1', # 125
    'formant\_5\_parallel.State.Z2', # 126

    'formant\_6\_parallel.Coef.A',   # 127
    'formant\_6\_parallel.Coef.B',   # 128
    'formant\_6\_parallel.Coef.C',   # 129
    'formant\_6\_parallel.State.Z1', # 130
    'formant\_6\_parallel.State.Z2', # 131

    # Resonators: Trach Pole Cascade
    'trach\_pole\_cascade.Coef.A',   # 132
    'trach\_pole\_cascade.Coef.B',   # 133
    'trach\_pole\_cascade.Coef.C',   # 134
    'trach\_pole\_cascade.State.Z1', # 135
    'trach\_pole\_cascade.State.Z2', # 136

    # Resonators: Formant Special 1 to 4
    'formant\_1\_special.Coef.A',    # 137
    'formant\_1\_special.Coef.B',    # 138
    'formant\_1\_special.Coef.C',    # 139
    'formant\_1\_special.State.Z1',  # 140
    'formant\_1\_special.State.Z2',  # 141

    'formant\_2\_special.Coef.A',    # 142
    'formant\_2\_special.Coef.B',    # 143
    'formant\_2\_special.Coef.C',    # 144
    'formant\_2\_special.State.Z1',  # 145
    'formant\_2\_special.State.Z2',  # 146

    'formant\_3\_special.Coef.A',    # 147
    'formant\_3\_special.Coef.B',    # 148
    'formant\_3\_special.Coef.C',    # 149
    'formant\_3\_special.State.Z1',  # 150
    'formant\_3\_special.State.Z2',  # 151

    'formant\_4\_special.Coef.A',    # 152
    'formant\_4\_special.Coef.B',    # 153
    'formant\_4\_special.Coef.C',    # 154
    'formant\_4\_special.State.Z1',  # 155
    'formant\_4\_special.State.Z2',  # 156

    # Resonators: nasal and trach pole special
    'nasal\_pole\_special.Coef.A',   # 157
    'nasal\_pole\_special.Coef.B',   # 158
    'nasal\_pole\_special.Coef.C',   # 159
    'nasal\_pole\_special.State.Z1', # 160
    'nasal\_pole\_special.State.Z2', # 161

    'trach\_pole\_special.Coef.A',   # 162
    'trach\_pole\_special.Coef.B',   # 163
    'trach\_pole\_special.Coef.C',   # 164
    'trach\_pole\_special.State.Z1', # 165
    'trach\_pole\_special.State.Z2', # 166

    # anti-resonators */
    'nasal\_zero\_cascade.Coef.A',   # 167
    'nasal\_zero\_cascade.Coef.B',   # 168
    'nasal\_zero\_cascade.Coef.C',   # 169
    'nasal\_zero\_cascade.State.Z1', # 170
    'nasal\_zero\_cascade.State.Z2', # 171

    'trach\_zero\_cascade.Coef.A',   # 172
    'trach\_zero\_cascade.Coef.B',   # 173
    'trach\_zero\_cascade.Coef.C',   # 174
    'trach\_zero\_cascade.State.Z1', # 175
    'trach\_zero\_cascade.State.Z2', # 176

    # 21 output parameters */
    'out[00] O\_NORMAL',            # 177
    'out[01] O\_VOICING',           # 178
    'out[02] O\_ASPIRATION',        # 179
    'out[03] O\_FRICATION',         # 180
    'out[04] O\_GLOTTAL',           # 181
    'out[05] O\_TRACHEAL\_CASC',     # 182
    'out[06] O\_NASAL\_ZERO\_CASC',   # 183
    'out[07] O\_NASAL\_POLE\_CASC',   # 184
    'out[08] O\_FORMANT\_5\_CASC'.replace('FORMANT\_','F'),    # 185
    'out[09] O\_FORMANT\_4\_CASC'.replace('FORMANT\_','F'),    # 186
    'out[10] O\_FORMANT\_3\_CASC'.replace('FORMANT\_','F'),    # 187
    'out[11] O\_FORMANT\_2\_CASC'.replace('FORMANT\_','F'),    # 188
    'out[12] O\_FORMANT\_1\_CASC'.replace('FORMANT\_','F'),    # 189
    'out[13] O\_FORMANT\_6\_PARA'.replace('FORMANT\_','F'),    # 190
    'out[14] O\_FORMANT\_5\_PARA'.replace('FORMANT\_','F'),    # 191
    'out[15] O\_FORMANT\_4\_PARA'.replace('FORMANT\_','F'),    # 192
    'out[16] O\_FORMANT\_3\_PARA'.replace('FORMANT\_','F'),    # 193
    'out[17] O\_FORMANT\_2\_PARA'.replace('FORMANT\_','F'),    # 194
    'out[18] O\_FORMANT\_1\_PARA'.replace('FORMANT\_','F'),    # 195
    'out[19] O\_NASAL\_PARA',        # 196
    'out[20] O\_BYPASS\_PARA',       # 197
]

assert len(MEM_PARAMS) == 198, 'mem params must be a 198-values-long array'

ORIGINAL_MAPPING = [
    ParamsSet([(0,   24)],  (5, 5), 'Synthesizer',                               transpose=False), # 1  (24  - 0   + 1 = 25) 25  
    ParamsSet([(25,  51)],  (4, 8), 'Coefficients + Speaker',                    transpose=False), # 2  (51  - 25  + 1 = 27) 28   
    ParamsSet([(52,  61)],  (5, 2), 'Resonators: glottal pulse + spectral tilt', transpose=True),  # 3  (61  - 52  + 1 = 10) 10 # transpose
    ParamsSet([(62,  66)],  (5, 1), 'Resonators: nasal pole cascade',            transpose=True),  # 4  (66  - 62  + 1 = 5 )  5 # transpose
    ParamsSet([(67,  106)], (5, 8), 'Resonators: formants cascade from 1 to 8',  transpose=True),  # 5  (106 - 67  + 1 = 40) 40 # transpose
    ParamsSet([(107, 131)], (5, 5), 'Resonators: formants parallel from 2 to 6', transpose=True),  # 6  (131 - 107 + 1 = 25) 25 # transpose
    ParamsSet([(132, 136)], (5, 1), 'Resonators: trach pole cascade',            transpose=True),  # 7  (136 - 132 + 1 = 5 )  5 # transpose
    ParamsSet([(137, 156)], (5, 4), 'Resonators: formants special from 1 to 4',  transpose=True),  # 8  (156 - 137 + 1 = 20) 20 # transpose
    ParamsSet([(157, 161)], (5, 1), 'Resonators: nasal pole special',            transpose=True),  # 9  (161 - 157 + 1 = 5 )  5 # transpose
    ParamsSet([(162, 166)], (5, 1), 'Resonators: trach pole special',            transpose=True),  # 10 (166 - 162 + 1 = 5 )  5 # transpose
    ParamsSet([(167, 171)], (5, 1), 'Resonators: nasal zero cascade',            transpose=True),  # 11 (171 - 167 + 1 = 5 )  5 # transpose
    ParamsSet([(172, 176)], (5, 1), 'Resonators: trach zero cascade',            transpose=True),  # 12 (176 - 172 + 1 = 5 )  5 # transpose
    ParamsSet([(177, 197)], (3, 7), 'Float: out',                                transpose=False), # 13 (197 - 177 + 1 = 21)  21  
]

num_params = 0
for ps in ORIGINAL_MAPPING:
    num_params += ps.num_params
assert num_params == 198, \
            'sum of number of parameters from all class objects ' \
            'does not match 198'

CONDENSED_MEM_MAP = [
    # 1  (24  - 0   + 1 = 25) 25
    # 2  (51  - 25  + 1 = 27) 28 
    ParamsSet( [(0,   24), (25,  51)],  (6, 9), 'Synthesizer + Coefficients + Speaker',  transpose=False), 
    # 3  (61  - 52  + 1 = 10) 10 # transpose
    # 5  (106 - 67  + 1 = 40) 40 # transpose
    ParamsSet( [(52,  61), (67,  106)], (5, 10), 'Resonators: Glottal Pulse + Spectral Tilt + F1 to F8 cascade',  transpose=True),
    # 6  (131 - 107 + 1 = 25) 25 # transpose
    # 13 (197 - 177 + 1 = 21) 21  
    ParamsSet( [(107, 131), (177, 197)], (5, 10), 'Resonators: F2 to F6 parallel + Float out', transpose=True),
    # 'Resonators: formants special from 1 to 4',  transpose=True),  # 8  (156 - 137 + 1 = 20) 20 # transpose
    # 'Resonators: nasal pole cascade' # 4  (66  - 62  + 1 = 5 )  5 # transpose
    # 'Resonators: nasal pole special' # 9  (161 - 157 + 1 = 5 )  5 # transpose
    # 'Resonators: nasal zero cascade' # 11 (171 - 167 + 1 = 5 )  5 # transpose
    # 'Resonators: trach pole cascade' # 7  (136 - 132 + 1 = 5 )  5 # transpose
    # 'Resonators: trach pole special' # 10 (166 - 162 + 1 = 5 )  5 # transpose
    # 'Resonators: trach zero cascade' # 12 (176 - 172 + 1 = 5 )  5 # transpose
    ParamsSet( [(137, 156), (62,  66), (157, 161), (167, 171), (132, 136), (162, 166), (172, 176)],  (5, 10), 'Resonators: F1 to F4 special + nasal vs trach: pole cascade, pole special and zero cascade',   transpose=True),
]

num_params = 0
for ps in CONDENSED_MEM_MAP:
    num_params += ps.num_params
assert num_params == 198, \
            'sum of number of parameters from all class objects ' \
            'does not match 198'
