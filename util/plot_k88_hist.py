#!/usr/bin/env python3
# -*- encoding: utf-8 -*-
# vim: set fileencoding=utf-8
#
# author: jan 2020
# cassio batista - https://cassota.gitlab.io/

import sys
import os
import glob
import time
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.ticker import FuncFormatter
from matplotlib import rc
from datetime import timedelta

from k88_handler import *

rc('font', **{'family': 'serif', 'sans-serif': ['Computer Modern Roman']})
rc('text', usetex=True)

DEBUG = False
NUM_HIST_BINS = 10
SHOW_BOXPLOT = True
SHOW_MEAN_STD = True
THRESH_CONSTANT = 0.980

MALE_SPEAKERS = ['paul', 'harry', 'frank', 'dennis']
FEMALE_SPEAKERS = ['betty', 'kit', 'rita', 'ursula', 'wendy']

SRC_DIR = os.path.dirname(os.path.abspath(__file__))
ROOT_DIR = os.path.abspath(os.path.dirname(SRC_DIR))

TAG = os.path.basename(sys.argv[0])


def create_mem_matrix(target, data_dir):
    mem_mtx = np.zeros((1, 48))
    if target == 'female':
        if DEBUG:
            spk_list = FEMALE_SPEAKERS[:1]
        else:
            spk_list = FEMALE_SPEAKERS
    elif target == 'male':
        if DEBUG:
            spk_list = MALE_SPEAKERS[:1]
        else:
            spk_list = MALE_SPEAKERS
    else:
        print('[%s] error: unrecognised target "%s"', (TAG, target))
        return None
    for spk in spk_list:
        spk_path = os.path.join(data_dir, spk)
        print('[%s] info: %s: scanning "%s"' % (TAG, target, spk_path))
        for mem_file in glob.glob(os.path.join(spk_path, '*.k88')):
            with open(mem_file, 'r') as f:
                lines = (line for line in f if not line.startswith('F0'))
                mem_mtx = np.concatenate((mem_mtx, np.loadtxt(lines, skiprows=1)))
    return mem_mtx.T  # one param per mtx line


# https://matplotlib.org/examples/pylab_examples/custom_ticker1.html
def thou_y(y, pos):
    if y >= 1000:
        return '%1.1fK' % (y / 1000)
    return '%d' % int(y)


def mill_y(y, pos):
    return '%1.0fM' % (y / 1000000)


def milli_x(x, pos):
    if x == 0.0:
        return '%d' % int(x)
    x_str = '%.2f' % (x * 1000)
    return x_str.strip('0').replace('-0.', '-.') + 'm'


def thou_x(x, pos):
    if x >= 1000 or x <= -1000:
        return '%1.1fK' % (x / 1000)
    return '%d' % int(x)


def leg_fmt(x):
    if x < 100 and x > -100:
        return '%1.2f' % x
    elif x >= 1000 or x <= -1000:
        return '%1.2fK' % (x / 1000)
    else:
        return '%d' % int(x)


def build_transpose_indexes(rows, cols):
    dim = rows * cols
    indexes = list(range(1, dim + 1))
    indexes_T = np.array(indexes).reshape((rows, cols)).T.flatten()
    return list(indexes_T)


def plot_hist(mtx_m, mtx_f, ps):
    fig = plt.figure()
    #plt.title(ps.title, fontsize=14)
    plt.axis('off')
    rows, cols = ps.rows, ps.cols
    for i, param_index in enumerate(ps.param_list):
        sys.stderr.write('  debug: subplot %-2d / %-2d (%-2d) ' %
                         (i + 1, ps.num_params, param_index))
        ax = fig.add_subplot(rows, cols, i + 1)
        array_m = mtx_m[param_index]
        array_f = mtx_f[param_index]
        mean_m, stddev_m = np.mean(array_m), np.std(array_m)
        mean_f, stddev_f = np.mean(array_f), np.std(array_f)
        # https://stackoverflow.com/a/23617505/3798300
        bins = np.histogram(np.hstack((array_m, array_f)),
                            bins=NUM_HIST_BINS * 2)[1]
        hist_m = ax.hist(array_m, bins=bins, fc=PRIM_COLOR['male'],
                         alpha=TRANSPAR['male'], ec=PRIM_COLOR['male'])
        hist_f = ax.hist(array_f, bins=bins, fc=PRIM_COLOR['female'],
                         alpha=TRANSPAR['female'], ec=PRIM_COLOR['female'])
        max_bin_percent_m = hist_m[0].max() / hist_m[0].sum()
        max_bin_percent_f = hist_f[0].max() / hist_f[0].sum()
        sys.stderr.write('%-10s ' % PARAMS[param_index])
        sys.stderr.write('%16.8f (μ) %16.8f (σ) %16.8f (↓) %16.8f (↑)\t| ' %
                         (mean_m, stddev_m, array_m.min(), array_m.max()))
        sys.stderr.write('%16.8f (μ) %16.8f (σ) %16.8f (↓) %16.8f (↑)\t' %
                         (mean_f, stddev_f, array_f.min(), array_f.max()))
        sys.stderr.write('[%6.2f %6.2f]\n' %
                         (max_bin_percent_m * 100, max_bin_percent_f * 100))
        sys.stderr.flush()
        if max_bin_percent_m > THRESH_CONSTANT and \
                max_bin_percent_f > THRESH_CONSTANT and \
                abs(mean_m - mean_f) < 1.0 - THRESH_CONSTANT:
            leg_1_fc = '#ff3333'
            for axis in ['bottom', 'top', 'left', 'right']:
                ax.spines[axis].set_linewidth(2.2)
                ax.spines[axis].set_color(leg_1_fc)
        else:
            leg_1_fc = 'white'
        if SHOW_MEAN_STD:
            height = np.mean(ax.get_ylim()) - 2 * np.mean(ax.get_ylim())/4
            height += np.mean(ax.get_ylim())/12
            label = r'$\mu\approx${}, {}{}$\sigma\approx${}, {}'
            eb_m = ax.errorbar(x=mean_m, y=height, xuplims=True, xlolims=True,
                               xerr=stddev_m, capsize=2, markersize=3, fmt='o',
                               ecolor=SEC_COLOR['male'],
                               mfc=SEC_COLOR['male'],
                               mec=SEC_COLOR['male'],
                               label=label.format(leg_fmt(mean_m),
                                                  leg_fmt(mean_f),
                                                  '\n',  # FIXME hehe vsf
                                                  leg_fmt(stddev_m),
                                                  leg_fmt(stddev_f)))
            height = np.mean(ax.get_ylim()) - 3 * np.mean(ax.get_ylim())/4
            height += np.mean(ax.get_ylim())/12
            eb_f = ax.errorbar(x=mean_f, y=height, xuplims=True, xlolims=True,
                               xerr=stddev_f, capsize=2, markersize=3, fmt='o',
                               ecolor=SEC_COLOR['female'],
                               mfc=SEC_COLOR['female'],
                               mec=SEC_COLOR['female'])
        if SHOW_BOXPLOT:
            pos = np.mean(ax.get_ylim()) - 2 * np.mean(ax.get_ylim())/4
            box_m = ax.boxplot(array_m, vert=False, positions=[pos],
                               widths=np.mean(ax.get_ylim())/6,
                               boxprops=dict(color=SEC_COLOR['male']),
                               capprops=dict(color=SEC_COLOR['male']),
                               whiskerprops=dict(color=SEC_COLOR['male']),
                               medianprops=dict(color=SEC_COLOR['male']),
                               flierprops=dict(marker='.', ms=0.01, alpha=0.5,
                                               mfc=PRIM_COLOR['male'],
                                               mec=PRIM_COLOR['male']))
            pos = np.mean(ax.get_ylim()) - 3 * np.mean(ax.get_ylim())/4
            box_f = ax.boxplot(array_f, vert=False, positions=[pos],
                               widths=np.mean(ax.get_ylim())/6,
                               boxprops=dict(color=SEC_COLOR['female']),
                               capprops=dict(color=SEC_COLOR['female']),
                               whiskerprops=dict(color=SEC_COLOR['female']),
                               medianprops=dict(color=SEC_COLOR['female']),
                               flierprops=dict(marker='.', ms=0.01, alpha=0.5,
                                               mfc=PRIM_COLOR['female'],
                                               mec=PRIM_COLOR['female']))
        # format yticks
        ax.set_yticks(np.arange(np.mean(ax.get_ylim()),
                                ax.get_ylim()[1] + 1,
                                np.mean(ax.get_ylim())))
        ax.set_ylim(ax.get_ylim())
        plt.yticks(rotation='vertical')
        # format yaxis
        if ax.get_ylim()[1] > 1000000:
            ax.yaxis.set_major_formatter(FuncFormatter(mill_y))
        elif ax.get_ylim()[1] > 1000:
            ax.yaxis.set_major_formatter(FuncFormatter(thou_y))
        # format xaxis
        if ax.get_xlim()[1] >= 1000000 or ax.get_xlim()[0] <= -1000000:
            ax.xaxis.set_major_formatter(FuncFormatter(mill_y))
        elif ax.get_xlim()[1] >= 1000 or ax.get_xlim()[0] <= -1000:
            ax.xaxis.set_major_formatter(FuncFormatter(thou_x))
        elif ax.get_xlim()[1] < (1 / 1000):
            ax.xaxis.set_major_formatter(FuncFormatter(milli_x))
        # https://stackoverflow.com/a/38454818/3798300
        leg_1 = plt.legend(handles=None, labels=[PARAMS[param_index].replace('formant_', 'F')],
                           markerscale=0, loc='upper right',
                           handlelength=0.0, handletextpad=0.1,
                           facecolor=leg_1_fc)
        handles, labels = ax.get_legend_handles_labels()
        leg_2 = ax.legend(handles=[h[0] for h in handles], labels=labels,
                          markerscale=0, fontsize=9, loc='upper left',
                          handlelength=0.0, handletextpad=0.1)
        ax.add_artist(leg_1)
    plt.subplots_adjust(left=0.025, right=0.975, top=0.950, bottom=0.05)
    fig.legend(handles=(hist_m[2][0], hist_f[2][0]), labels=('male', 'female'),
               ncol=2)
    plt.show(block=False)


if __name__ == '__main__':
    if len(sys.argv) != 2:
        print('usage: %s <target-gen> <data-dir>' % TAG)
        print('  <data-dir> dir where data for all speakers has been stored')
        sys.exit(1)

    data_dir = sys.argv[1]
    if not os.path.isdir(data_dir):
        print('[%s] error: "%s" is not a valid dir' % (TAG, data_dir))
        sys.exit(1)

    if DEBUG:
        print('[%s] warning: running in debug mode' % TAG)
    if SHOW_BOXPLOT:
        print('[%s] info: boxplot will be shown over histogram' % TAG)
    if SHOW_MEAN_STD:
        print('[%s] info: μ and σ will be shown over histogram' % TAG)

    start_time = time.time()
    mem_mtx_m = create_mem_matrix('male', data_dir)
    mem_mtx_f = create_mem_matrix('female', data_dir)
    for i, ps in enumerate(CONDENSED_PARAMS_MAP):
        print('[%s] info: plotting param set %d: %s' % (TAG, i + 1, ps.title))
        plot_hist(mem_mtx_m, mem_mtx_f, ps)
    end_time = time.time()
    elapsed_time = str(timedelta(seconds=end_time - start_time))
    print('[%s] info: done! time: %s' % (TAG, elapsed_time))
    input('[%s] info: type enter to close everything: ' % TAG)
