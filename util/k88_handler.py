#!/usr/bin/env python3
# -*- encoding: utf-8 -*-
# vim: set fileencoding=utf-8
#
# author: jan 2020
# cassio batista - https://cassota.gitlab.io/

PRIM_COLOR = { 'male': 'C0', 'female': 'C1' }
SEC_COLOR  = { 'male': 'b', 'female': 'r'}
TRANSPAR   = { 'male': 0.7, 'female': 0.3}


class ParamsSet:
    def __init__(self, rng_list, dim, title, transpose=False):
        self.ranges_list  = rng_list
        self.rows         = dim[0] 
        self.cols         = dim[1]
        self.dim          = self.rows * self.cols
        self.title        = title
        self.transpose    = transpose

        self.do_assert()

        self.param_list = self.gen_param_list()
        self.num_params = len(self.param_list)

    def do_assert(self):
        assert isinstance(self.ranges_list, list), \
                    'params index must be a LIST of tuples'
        assert isinstance(self.rows, int), \
                   'number of rows must be an INT'
        assert isinstance(self.cols, int), \
                    'number of cols must be an INT'
        assert isinstance(self.title, str), \
                    'title must be a string'

    def gen_param_list(self):
        param_list = []
        for rng in self.ranges_list:
            begin = rng[0]
            end = rng[1] + 1
            param_list += list(range(begin, end))
        return param_list

PARAMS = [
    'F0',    # 00
    'AV',    # 01
    'OQ',    # 02
    'SQ',    # 03
    'TL',    # 04
    'FL',    # 05
    'DI',    # 06
    'AH',    # 07
    'AF',    # 08
    'F1',    # 09
    'B1',    # 10
    'DF1',   # 11
    'DB1',   # 12
    'F2',    # 13
    'B2',    # 14
    'F3',    # 15
    'B3',    # 16
    'F4',    # 17
    'B4',    # 18
    'F5',    # 19
    'B5',    # 20
    'F6',    # 21
    'B6',    # 22
    'FNP',   # 23
    'BNP',   # 24
    'FNZ',   # 25
    'BNZ',   # 26
    'FTP',   # 27
    'BTP',   # 28
    'FTZ',   # 29
    'BTZ',   # 30
    'A2F',   # 31
    'A3F',   # 32
    'A4F',   # 33
    'A5F',   # 34
    'A6F',   # 35
    'AB',    # 36
    'B2F',   # 37
    'B3F',   # 38
    'B4F',   # 39
    'B5F',   # 40
    'B6F',   # 41
    'ANV',   # 42
    'A1V',   # 43
    'A2V',   # 44
    'A3V',   # 45
    'A4V',   # 46
    'ATV',   # 47
]

assert len(PARAMS) == 48, 'params must be a 48-values-long array'

CONDENSED_PARAMS_MAP = [
    ParamsSet( [(0, 47)],  (6, 8), '48 Klatt Parameters', transpose=False)
]

num_params = 0
for ps in CONDENSED_PARAMS_MAP:
    num_params += ps.num_params
assert num_params == 48, \
            'sum of number of parameters from all class objects ' \
            'does not match 48'
