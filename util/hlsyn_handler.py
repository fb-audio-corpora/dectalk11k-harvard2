#!/usr/bin/env python3
# -*- encoding: utf-8 -*-
# vim: set fileencoding=utf-8
#
# author: jan 2020
# cassio batista - https://cassota.gitlab.io/

PRIM_COLOR = { 'male': 'C0', 'female': 'C1' }
SEC_COLOR  = { 'male': 'b', 'female': 'r'}
TRANSPAR   = { 'male': 0.7, 'female': 0.3}


class ParamsSet:
    def __init__(self, rng_list, dim, title, transpose=False):
        self.ranges_list  = rng_list
        self.rows         = dim[0] 
        self.cols         = dim[1]
        self.dim          = self.rows * self.cols
        self.title        = title
        self.transpose    = transpose

        self.do_assert()

        self.param_list = self.gen_param_list()
        self.num_params = len(self.param_list)

    def do_assert(self):
        assert isinstance(self.ranges_list, list), \
                    'params index must be a LIST of tuples'
        assert isinstance(self.rows, int), \
                   'number of rows must be an INT'
        assert isinstance(self.cols, int), \
                    'number of cols must be an INT'
        assert isinstance(self.title, str), \
                    'title must be a string'

    def gen_param_list(self):
        param_list = []
        for rng in self.ranges_list:
            begin = rng[0]
            end = rng[1] + 1
            param_list += list(range(begin, end))
        return param_list

PARAMS = [
    'AG',   # 00
    'AL',   # 01
    'AB',   # 02
    'AN',   # 03
    'UE',   # 04
    'F0',   # 05
    'F1',   # 06
    'F2',   # 07
    'F3',   # 08
    'F4',   # 09
    'PS',   # 10
    'DC',   # 11
    'AP',   # 12
]

assert len(PARAMS) == 13, 'params must be a 13-values-long array'

CONDENSED_PARAMS_MAP = [
    ParamsSet( [(0, 12)],  (5, 3), '13 HLSyn Parameters', transpose=False)
]

num_params = 0
for ps in CONDENSED_PARAMS_MAP:
    num_params += ps.num_params
assert num_params == 13, \
            'sum of number of parameters from all class objects ' \
            'does not match 13'
