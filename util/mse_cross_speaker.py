#!/usr/bin/env python3
#
# a script that plots some visualisation of the MSE calculated between a speech
# waveform and a pure silence signal (all samples zeroed) in order to see how
# the similarity measure behaves in terms of numbers
#
# Grupo FalaBrasil (2020)
# Universidade Federal do Pará (UFPA)
#
# author: jan 2020
# cassio batista - https://cassota..gitlab.io/

import sys
import os
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.ticker import FuncFormatter
from matplotlib import rc
from sklearn.metrics import mean_squared_error


rc('font', **{'family': 'serif', 'sans-serif': ['Computer Modern Roman']})
rc('text', usetex=True)

DEBUG = False
TAG = sys.argv[0]

MILLION = 1000000.0
NUM_AUDIOS = 240
RAW_SUFFIX = '_hldata%d.k88.raw'
MALE = ['paul', 'harry', 'frank', 'dennis']
FEMALE = ['betty', 'kit', 'rita', 'ursula', 'wendy']

if DEBUG:
    SPEAKERS = MALE[:3] + FEMALE[:3]
else:
    SPEAKERS = MALE + FEMALE


def read_rawfile(rawname):
    with open(rawname, 'r+') as f:
        raw = np.fromfile(f, dtype=np.int16)
    return raw


# https://matplotlib.org/examples/pylab_examples/custom_ticker1.html
def mill_y(y, pos):
    if y < -MILLION:
        return '%1.0f' % np.float64(y / MILLION)
    return ''


if __name__ == '__main__':
    if len(sys.argv) != 2:
        print('usage: %s <target-gen> <data-dir>' % TAG)
        print('  <data-dir> dir where data for all speakers has been stored')
        sys.exit(1)

    data_dir = sys.argv[1]
    if not os.path.isdir(data_dir):
        print('[%s] error: "%s" is not a valid dir' % (TAG, data_dir))
        sys.exit(1)

    if DEBUG:
        print('[%s] warning: running in DEBUG mode' % TAG)

    fig = plt.figure()
    rows, cols = len(SPEAKERS), len(SPEAKERS)
    subplot_index = 0
    for i, x_speaker in enumerate(SPEAKERS):
        x_raw_prefix = os.path.join(data_dir, x_speaker, x_speaker)
        for j, y_speaker in enumerate(SPEAKERS):
            mse_list = []
            subplot_index += 1
            y_raw_prefix = os.path.join(data_dir, y_speaker, y_speaker)
            for k in range(NUM_AUDIOS):
                print('\r[%s] I: %02d scanning "%7s" x "%-7s"\t(audio %d)' %
                      (TAG, subplot_index, x_speaker, y_speaker, k), end=' ')
                x_filename = x_raw_prefix + RAW_SUFFIX % k
                y_filename = y_raw_prefix + RAW_SUFFIX % k
                x = read_rawfile(x_filename).astype(np.float64)
                y = read_rawfile(y_filename).astype(np.float64)
                if abs(x.shape[0] - y.shape[0]) > 0:
                    if x.shape[0] > y.shape[0]:
                        x = x[:y.shape[0]]
                    else:
                        y = y[:x.shape[0]]
                assert x.shape == y.shape, 'vish o sklearn vai reclamar'
                mse_val = -1 * mean_squared_error(x, y)
                mse_list.append(mse_val)
            ax = fig.add_subplot(rows, cols, subplot_index, frame_on=True)
            bp = ax.boxplot(mse_list, vert=False, widths=0.5,
                            patch_artist=True)
            for patch in bp['boxes']:
                patch.set_facecolor('w')
            ax.set_yticks([])
            ax.set_xlim((-50 * MILLION - 1000000, 0 + 1000000))
            ax.set_xticks(np.arange(-50 * MILLION, 1, 10 * MILLION))
            ax.grid()

            # format yaxis
            if ax.get_xlim()[0] < -1000000:
                ax.xaxis.set_major_formatter(FuncFormatter(mill_y))
            if subplot_index <= rows * (cols - 1):
                ax.set_xticklabels([])
            if subplot_index <= rows:
                ax.set_title(y_speaker.capitalize(), fontsize=11)
            if (subplot_index - 1) % cols == 0:
                ax.set_ylabel(x_speaker.capitalize(), fontsize=11)
        print()
    plt.gcf().text(0.971, 0.0300, r'$\times 10^6$', fontsize=11)
    plt.show(block=False)
    plt.subplots_adjust(left=0.025, right=0.975, top=0.950, bottom=0.05)
    x = input('[%s] done! press any key to close everything ')
