#!/usr/bin/env python3
#
# a script that plots some visualisation of the MSE calculated between a speech
# waveform and a pure silence signal (all samples zeroed) in order to see how
# the similarity measure behaves in terms of numbers
#
# Grupo FalaBrasil (2020)
# Universidade Federal do Pará (UFPA)
#
# author: jan 2020
# cassio batista - https://cassota..gitlab.io/

import sys
import os
import glob
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.ticker import FuncFormatter
from matplotlib import rc
from sklearn.metrics import mean_squared_error


rc('font', **{'family': 'serif', 'sans-serif': ['Computer Modern Roman']})
rc('text', usetex=True)

DEBUG = False
TAG = sys.argv[0]


MALE = ['paul', 'harry', 'frank', 'dennis']
FEMALE = ['betty', 'kit', 'rita', 'ursula', 'wendy']
SPEAKERS = MALE + FEMALE

if DEBUG:
    SPEAKERS = SPEAKERS[:2]


def read_rawfile(rawname):
    with open(rawname, 'r+') as f:
        raw = np.fromfile(f, dtype=np.int16)
    return raw


# https://matplotlib.org/examples/pylab_examples/custom_ticker1.html
def mill_y(y, pos):
    if y < -1000000:
        return '%1.0f' % np.float64(y / 1000000.0)
    return ''


if __name__ == '__main__':
    if len(sys.argv) != 2:
        print('usage: %s <target-gen> <data-dir>' % TAG)
        print('  <data-dir> dir where data for all speakers has been stored')
        sys.exit(1)

    data_dir = sys.argv[1]
    if not os.path.isdir(data_dir):
        print('[%s] error: "%s" is not a valid dir' % (TAG, data_dir))
        sys.exit(1)

    if DEBUG:
        print('[%s] warning: running in DEBUG mode' % TAG)

    ax = plt.axes()
    mean = []
    male_list = []
    female_list = []
    for i, spk in enumerate(SPEAKERS):
        mse_list = []
        spk_path = os.path.join(data_dir, spk)
        for j, raw_file in enumerate(glob.glob(os.path.join(spk_path,
                                                            '*.k88.raw'))):
            print('\r[%s] I: scanning "%6s"\t(audio %d)' % (TAG, spk_path, j),
                  end=' ')
            raw = read_rawfile(raw_file).astype(np.float64)
            sil = np.zeros(raw.shape, dtype=np.float64)
            assert raw.shape == sil.shape, 'array sizes do not match'
            mse_val = -1 * mean_squared_error(raw, sil)
            mse_list.append(mse_val)
            mean.append(mse_val)
            if spk in MALE:
                male_list.append(mse_val)
            else:
                female_list.append(mse_val)
        bp = ax.boxplot(mse_list, positions=[i], widths=0.5, patch_artist=True,
                        medianprops=dict(color='k', linewidth=3))
        for patch in bp['boxes']:
            patch.set_facecolor('w')
        print('ok!')

    bp = ax.boxplot(male_list, positions=[i + 1], patch_artist=True,
                    boxprops=dict(color='b', linewidth=2),
                    medianprops=dict(color='k', linewidth=3))
    for patch in bp['boxes']:
        patch.set_facecolor('w')
    bp = ax.boxplot(female_list, positions=[i + 2],  patch_artist=True,
                    boxprops=dict(color='r', linewidth=2),
                    medianprops=dict(color='k', linewidth=3))
    for patch in bp['boxes']:
        patch.set_facecolor('w')
    bp = ax.boxplot(mean, positions=[i + 3], patch_artist=True,
                    boxprops=dict(color='k', linewidth=2),
                    medianprops=dict(color='k', linewidth=3))
    for patch in bp['boxes']:
        patch.set_facecolor('w')

    mean = np.mean(mean)
    plt.plot((-0.5, len(SPEAKERS)+2.5), (mean, mean), 'g--', linewidth=2.0)

    plt.gcf().text(0.027, 0.95, r'$\times 10^6$', fontsize=14)

    # format yaxis
    if ax.get_ylim()[0] < -1000000:
        ax.yaxis.set_major_formatter(FuncFormatter(mill_y))
    plt.ylim([-30000000, 0])
    plt.grid()
    plt.xticks(ticks=np.arange(0, len(SPEAKERS)+3),
               labels=[s.capitalize() for s in SPEAKERS] +
                      [r'\_male\_', r'\_female\_', r'\_all\_'], fontsize=14)
    plt.yticks(rotation='vertical', verticalalignment='center', fontsize=16)
    plt.ylabel(r'$-1 \times$ MSE', fontsize=20)
    plt.show(block=False)
    plt.subplots_adjust(left=0.050, right=0.955, top=0.950, bottom=0.05)
    x = input('[%s] done! press any key to close everything ')
