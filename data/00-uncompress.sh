#!/bin/bash
#
# Grupo FalaBrasil (2020)
# Universidade Federal do Pará (UFPA)
#
# author: jan 2020
# cassio batista - https://cassota.gitlab.io/

if ! type -t tar > /dev/null ; then
    echo "[$0] error: to exec this script you must first install 'tar'"
    exit 1
fi

for dir in betty dennis frank harry kit paul rita ursula wendy ; do
    echo "[$0] uncompressing '$dir'..."
    tar -xzf ${dir}.tar.gz
done
echo "done!"
